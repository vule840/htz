import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import VueCroppie from 'vue-croppie'
import * as rules from 'vee-validate/dist/rules';
import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';
import { messages } from 'vee-validate/dist/locale/hr.json';
import VueAnalytics from 'vue-analytics';
import VueMeta from 'vue-meta';
import * as VueGoogleMaps from 'vue2-google-maps'
import * as URLSearchParams from 'url-search-params-polyfill';
// import './jquery.js'



Vue.use(VueMeta)
var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);

Vue.use(VueAnalytics, {
    id: 'UA-157752884-1'
})

Vue.config.productionTip = false

Vue.use(URLSearchParams);
Vue.use(BootstrapVue);
Vue.use(VueCroppie);
Vue.component('hamburger', require('vue-hamburger'));

Object.keys(rules).forEach(rule => {
    extend(rule,

        {
            ...rules[rule], // copies rule configuration
            message: messages[rule] // assign message
        }

    );
});
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
var SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyAgJg2_WbaoCpuXeR1sANObCdnrLWiuDaI',
      libraries: 'visualization',
    },
    installComponents: true
  })


new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')