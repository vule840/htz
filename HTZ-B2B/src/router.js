import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Meta from 'vue-meta'

import About from './views/About'
import KakoSudjelovati from './views/KakoSudjelovati'
import Kategorije from './views/Kategorije'
import CestaPitanja from './views/CestaPitanja'
import Admin from './views/Admin'
import Pravila from "./views/Pravila"
import Kolacici from "./views/Kolacici"
import Uvjeti from "./views/Uvjeti"

import Cropper from './components/Cropper'

// user
import Prijava from './components/user/Prijava'
import Dashboard from './components/user/Dashboard'
import PregledPonude from './components/user/PregledPonude'
import Registracija from "./components/Registracija.vue"
import Ponude from "./components/user/Ponude"
import IzmjenaPonude from "./components/user/IzmjenaPonude"
import notFound from "./views/notFound"
import RegistracijaZavrsena from "./components/RegistracijaZavrsena"
//import store from './store'

import PregledPonudeAdmin from "@/components/admin/PregledPonudeAdmin.vue";


Vue.use(Router)
Vue.use(Meta)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/about',
      component: About
    },
    {
      path: '/',
      component: Home
    },
    {
      path: '/kako-sudjelovati',
      component: KakoSudjelovati
    },
    {
      path: '/kategorije',
      component: Kategorije
    },
    {
      path: '/cesta-pitanja',
      component: CestaPitanja
    },
    {
      path: '/admin',
      component: Admin
    },
    {
      path: '/prijava',
      component: Prijava
    },
    {
      path: '/dashboard',
      component: Dashboard
    },
    {
      path: '/cropper',
      component: Cropper
    },

    {
      path: '/pregled-ponude/:id',
       name: 'pregled-ponude',
      component: PregledPonude,
       props: true
    },
     {
      path: '/admin-pregled-ponude/:id',
       name: 'admin-pregled-ponude',
      component: PregledPonudeAdmin,
       props: true
    },

    {
      path: '/ponude',
      component: Ponude
    },

    {
      path: '/registracija',
      component: Registracija
    },
    {
      path: '/registracija-zavrsena',
      component: RegistracijaZavrsena
    },
    {
      path: '/pravila-i-uvjeti-sudjelovanja',
      component: Pravila
    },
    {
      path: '/politika-kolacica',
      component: Kolacici
    },
    {
      path: '/uvjeti-koristenja',
      component: Uvjeti
    },
    {
      path: '*',
      component: notFound
    },
    {
       path: '/izmjena-ponude/:id', 
      name: 'izmjena-ponude',
      component: IzmjenaPonude,
      props: true
    }
  ]
}) 