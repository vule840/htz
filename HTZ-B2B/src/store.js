import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { get } from 'https'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex, VueAxios, axios)
//baseUrl nije definiran kod nekih templatea(Dashboard - user, Ponude ...) jer vj. beforeMounted pa se nemože iz store-a loadti variable
export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
      baseUrl: "https://env-1254868.de-fra1.cloudjiffy.net",
      email: '',
      pass: '',
      token: '',
      hideCookie: false,
      isLoggedIn: false 
      },
  mutations: {
      setEmailPass(state, userInfo) {
        state.email = userInfo.email;
        state.pass = userInfo.pass
      },
      setToken(state, response) {
        state.isLoggedIn = true
        state.token = response;
        
      },
      changehideCookie(state, value){
      state.hideCookie = value;
      },
      logOut(state, payload){
        state.token = null
        state.isLoggedIn = false
       localStorage.removeItem('token', null)
      
      },

  },
  actions: {
    }
  })
