module.exports = {
	 publicPath: process.env.NODE_ENV === 'production'
    ? '/B2B'
    : '/',
    devServer: {
        proxy: 'https://cloudjiffy.net/',
    }
  }    