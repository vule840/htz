import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import MainLayout from "./layouts/mainLayout.vue";
import OfferLayout from "./layouts/offerLayout.vue";
// import VueAnalytics from 'vue-analytics';
import BootstrapVue from 'bootstrap-vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import * as URLSearchParams from 'url-search-params-polyfill';
// import './gtm.js'
import VueCarousel from 'vue-carousel';
import BackToTop from 'vue-backtotop'
 
Vue.use(BackToTop)

var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);
Vue.component('main-layout', MainLayout);
Vue.component('offer-layout', OfferLayout);
Vue.component('hamburger', require('vue-hamburger'))

// Vue.use(VueAnalytics, {
//   id: 'UA-157752884-2'
// })

 
Vue.use(VueCarousel);
Vue.use(BootstrapVue);
Vue.use(URLSearchParams);

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyC-kvcJL-wQ6CN458FQG2eLCALXEp0UVb4',
    libraries: 'visualization',
  },
  installComponents: true
})

Vue.config.productionTip = false

import VueSocialSharing from 'vue-social-sharing';
Vue.use(VueSocialSharing);

//SCROLL TO
var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo, {
  // container: "body",
  duration: 1500,
  easing: "ease",
  // offset: 0,
  // force: true,
  // cancelable: true,
  // onStart: false,
  // onDone: false,
  // onCancel: false,
  // x: false,
  // y: true
})

import LoadScript from 'vue-plugin-load-script';
 
Vue.use(LoadScript);
// import VueGtm from 'vue-gtm';
// import VueRouter from 'vue-router';
// //const routers = new router({ routes, mode, linkActiveClass });
// const router = new VueRouter({ routes, mode, linkActiveClass });

// Vue.use(VueGtm, {
//   id: 'GTM-NN73S2S', // Your GTM single container ID or array of container ids ['GTM-xxxxxx', 'GTM-yyyyyy']
//   queryParams: { // Add url query string when load gtm.js with GTM ID (optional)
//     gtm_auth:'AB7cDEf3GHIjkl-MnOP8qr',
//     gtm_preview:'env-4',
//     gtm_cookies_win:'x'
//   },
//   defer: false, // defaults to false. Script can be set to `defer` to increase page-load-time at the cost of less accurate results (in case visitor leaves before script is loaded, which is unlikely but possible)
//   enabled: true, // defaults to true. Plugin can be disabled by setting this to false for Ex: enabled: !!GDPR_Cookie (optional)
//   debug: true, // Whether or not display console logs debugs (optional)
//   loadScript: true, // Whether or not to load the GTM Script (Helpful if you are including GTM manually, but need the dataLayer functionality in your components) (optional)
//   router: router, //Pass the router instance to automatically sync with router (optional)
//   ignoredViews: ['homepage'], // Don't trigger events for specified router names (case insensitive) (optional)
//   trackOnNextTick: false, // Whether or not call trackView in Vue.nextTick
// });


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
