import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Uvjeti from './views/Uvjeti.vue'
import Kolacici from "./views/Kolacici"
import Pravila from "./views/Pravila"
import CestaPitanja from './views/CestaPitanja'
import Meta from 'vue-meta'


Vue.use(Meta, {
  keyName: 'metaInfo',
  attribute: 'data-vue-meta',
  ssrAttribute: 'data-vue-meta-server-rendered',
  tagIDKeyName: 'vmid',
  refreshOnceOnNavigation: true
})

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/Akcija',
      name: 'Akcija',
      component: () => import('./views/Akcija.vue')
    },
    {
      path: '/uvjeti-koristenja',
      component: Uvjeti,
      component: () => import('./views/Uvjeti.vue')
    },
    {
      path: '/politika-kolacica',
      component: Kolacici
    },
    {
      path: '/cesta-pitanja',
      name: 'CestaPitanja',
      component: CestaPitanja
    },
    {
      path: '/pravila-i-uvjeti-sudjelovanja',
      component: Pravila
    },
    // path: '/B2B',
    // beforeEnter() {location.href = 'https://tjedanodmoravrijedan.hr/B2B/'}
    // }
    // {
    //   path: '/ponuda/:id',
    //   name: 'ponuda',
    //   component: () => import('./views/Ponuda.vue'),
    //   props: true,
    //   meta: { layout: "offer"}
    // }

  ]
})
