import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// import VueAxios from 'vue-axios'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	baseUrl: "https://env-1254868.de-fra1.cloudjiffy.net",
    hideCookie: false
  },
  mutations: {
    changehideCookie(state, value){
      state.hideCookie = value;
      }
 
  },
  actions: {



  }
})
