<template>
  <div class="offer">
    <div class="cofferHolderSingle">
      <div class="postHolder">
        <div class="fullPost">
          <div class="topContentHolder">
            <div class="left">
              <div class="sectionLeftHolder" v-if="selectedSection == 1">
                <!-- GALLERY SLICK SLIDER -->
                <slick
                  ref="slick"
                  :options="slickOptions"
                  @afterChange="handleAfterChange"
                  @beforeChange="handleBeforeChange"
                  @breakpoint="handleBreakpoint"
                  @destroy="handleDestroy"
                  @edge="handleEdge"
                  @init="handleInit"
                  @reInit="handleReInit"
                  @setPosition="handleSetPosition"
                  @swipe="handleSwipe"
                  @lazyLoaded="handleLazyLoaded"
                  @lazyLoadError="handleLazeLoadError"
                >
                  <img v-bind:src="offer.slikaGlavna.putanjaSlike" />
                  <img
                    v-for="(image,index) in offer.slikaSporedne"
                    :key="index"
                    v-bind:src="image.putanjaSlike"
                    alt
                    style="width: 100%; height:100%;"
                  />
                </slick>
                <!-- /GALLERY SLICK SLIDER -->
              </div>

              <!-- GOOGLE MAPS SECTION -->
              <div class="sectionLeftHolder" v-if="selectedSection == 3">
                <!-- GOOGLE MAP -->
                <GmapMap
                  :center="center"
                  :options="{mapTypeControl: false}"
                  :zoom="15"
                  style="width: 100%; height: 500px"
                >
                  <GmapMarker
                    :key="index"
                    v-for="(m, index) in markers"
                    :position="m.position"
                    :clickable="true"
                    :draggable="true"
                    @click="center=m.position"
                  />
                </GmapMap>
              </div>
            </div>
            <div class="right">
              <div class="right-cta">
                <div class="right-cta__item right-cta__item--mapa">
                  <a @click="selectedSection = 3" class="cta-google-map">
                    <svg
                      width="32"
                      height="39"
                      viewBox="0 0 32 39"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M15.6266 25.2098C20.9193 25.2098 25.2098 20.9193 25.2098 15.6266C25.2098 10.334 20.9193 6.04346 15.6266 6.04346C10.334 6.04346 6.04346 10.334 6.04346 15.6266C6.04346 20.9193 10.334 25.2098 15.6266 25.2098Z"
                        fill="white"
                      />
                      <path
                        d="M15.6266 18.7809C17.2834 18.7809 18.6266 17.4378 18.6266 15.7809C18.6266 14.1241 17.2834 12.7809 15.6266 12.7809C13.9697 12.7809 12.6266 14.1241 12.6266 15.7809C12.6266 17.4378 13.9697 18.7809 15.6266 18.7809Z"
                      />
                      <path
                        d="M15.6266 0C6.99314 0 0 6.99313 0 15.6266C0 22.4471 9.78463 33.4692 13.8712 37.7859C14.8208 38.7932 16.4324 38.7932 17.3821 37.7859C21.4686 33.498 31.2533 22.4759 31.2533 15.6266C31.2533 6.99313 24.2601 0 15.6266 0ZM15.6266 24.2889C10.8207 24.2889 6.96436 20.4038 6.96436 15.6266C6.96436 10.8494 10.8494 6.96435 15.6266 6.96435C20.4326 6.96435 24.2889 10.8494 24.2889 15.6266C24.2889 20.4038 20.4326 24.2889 15.6266 24.2889Z"
                      />
                    </svg>
                    <span>Lokacija</span>
                  </a>
                </div>
                <div class="right-cta__item right-cta__item--galerija">
                  <a @click="selectedSection = 1" class="cta-gallery">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 32 32"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <rect
                        x="1"
                        y="1"
                        width="30"
                        height="30"
                        rx="1"
                        stroke="#fff"
                        stroke-width="2"
                      />
                      <rect x="5.00012" y="5.00125" width="8" height="10" fill="#fff" />
                      <rect x="15.9999" y="5.00125" width="11" height="10" fill="#fff" />
                      <rect x="19.9999" y="18.0009" width="7" height="9" fill="#fff" />
                      <rect x="5.00012" y="18.0009" width="12" height="9" fill="#fff" />
                    </svg>
                    <span>Galerija</span>
                  </a>
                </div>
              </div>
              <div v-if="Object.keys(offer.poslovnaJedinica).length > 0">
                <ul class="offer-contact">
                  <li class="offer-contact__item offer-contact__item--adresa">
                    <a
                      @click="selectedSection = 3"
                    >{{offer.poslovnaJedinica.adresa}}, {{offer.poslovnaJedinica.mjesto}}</a>
                  </li>
                  <li class="offer-contact__item offer-contact__item--telefon">
                    <a
                      v-bind:href="'tel:' + offer.poslovnaJedinica.telefon"
                    >{{offer.poslovnaJedinica.telefon}}</a>
                  </li>
                  <li class="offer-contact__item offer-contact__item--email">
                    <a
                      v-bind:href="'mailto:' + offer.poslovnaJedinica.email"
                      @click="openLink"
                    >{{offer.poslovnaJedinica.email}}</a>
                  </li>
                  <li
                    v-if="offer.poslovnaJedinica.web"
                    class="offer-contact__item offer-contact__item--web"
                  >
                    <a
                      v-bind:href="offer.poslovnaJedinica.web | cleanURL"
                      target="_blank"
                      @click="openLink"
                    >Posjeti web stranicu</a>
                  </li>
                </ul>
              </div>

              <div v-else>
                <ul class="offer-contact">
                  <li class="offer-contact__item offer-contact__item--adresa">
                    <a
                      @click="selectedSection = 3"
                    >{{offer.poslovniSubjekt.adresa}}, {{offer.poslovniSubjekt.mjesto}}</a>
                  </li>
                  <li class="offer-contact__item offer-contact__item--telefon">
                    <a
                      v-bind:href="'tel:' + offer.poslovniSubjekt.telefon"
                    >{{offer.poslovniSubjekt.telefon}}</a>
                  </li>
                  <li class="offer-contact__item offer-contact__item--email">
                    <a
                      v-bind:href="'mailto:' + offer.poslovniSubjekt.email"
                      @click="openLink"
                    >{{offer.poslovniSubjekt.email}}</a>
                  </li>
                  <li
                    v-if="offer.poslovniSubjekt.web"
                    class="offer-contact__item offer-contact__item--web"
                  >
                    <a
                      v-bind:href="offer.poslovniSubjekt.web | cleanURL"
                      target="_blank"
                      @click="openLink"
                    >Posjeti web stranicu</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="botContentHolder">
            <!--  <h1 v-if="offer.poslovniSubjekt" class="offerTitle">{{offer.poslovniSubjekt.naziv}}</h1>
            <h1 v-if="offer.poslovnaJedinica" class="offerTitle">{{offer.poslovnaJedinica.naziv}}</h1>-->
            <h1
              class="offerTitle"
              v-if="Object.keys(offer.poslovnaJedinica).length > 0"
            >{{offer.poslovnaJedinica.naziv}}</h1>
            <h1
              class="offerTitle"
              v-if="Object.keys(offer.poslovnaJedinica).length === 0"
            >{{offer.poslovniSubjekt.naziv}}</h1>
            <p class="offerText" style="white-space: pre-wrap;">{{offer.opisPonude}}</p>
            <p class="subText">{{offer.napomena}}</p>

            <!-- <div class="cta-social-group">
                            <a :href="'http://www.facebook.com/sharer.php?u=https://tjedanodmoravrijedan.hr/ponuda/' + offer.id + '&t=' + offer.opisPonude" class="cta-social cta-social--fb"><img src="../assets/fb_logo2.svg"> Facebook</a>
                            <a :href="'https://twitter.com/intent/tweet?text=,%20RT%20https://tjedanodmoravrijedan.hr/ponuda/' + offer.id" class="cta-social cta-social--tw"><img src="../assets/tw_logo.svg"> Twitter</a>
            </div>-->

            <!-- <social-sharing
                            :url="'http://tjedanodmoravrijedan.hr/ponuda/' + this.offer.id"
                            :title="this.offer.poslovniSubjekt.naziv"
                            :description="this.offer.napomena"
                            :quote="this.offer.opisPonude"
                            hashtags="htz, tjedanodmoravrijedan"
                            twitter-user="htz, tjedanodmoravrijedan"
                            network-tag="span"
                            inline-template
            >-->
            <div
              class="fb-like"
              data-href="https://test.com/post/${post.slug}"
              data-width
              data-layout="button"
              data-action="like"
              data-size="small"
              data-share="true"
            >sdfsd</div>

            <iframe
              src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fdigital.bbdo.hr%2FHTZ-B2C%2Fponuda%2F136&appId=23012578568&width=96&height=20"
              width="96"
              height="20"
              style="border:none;overflow:hidden"
              scrolling="no"
              frameborder="0"
              allowtransparency="true"
              allow="encrypted-media"
            ></iframe>
            <a href="https://www.facebook.com/sharer.php">Sharer2 Iframe</a>
            <div class="cta-social-group">
              <!-- <iframe src="https://www.facebook.com/plugins/share_button.php?href=https://tjedanodmoravrijedan.hr/ponuda/136&width=96&height=20" width="96" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe> -->

             

             
              <a
                href="javascript:fbShare('http://jsfiddle.net/stichoza/EYxTJ/', 'Fb Share', 'Facebook share popup', 'http://goo.gl/dS52U', 520, 350)"
              >Share</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>


<script>
import axios from "axios";
import Slick from "vue-slick";
export default {
  props: ["id"],
  components: {
    Slick,
  },
  data: function () {
    return {
      url: "",
      center: {
        lat: 45.814464,
        lng: 15.98286,
      },
      markers: [
        {
          position: {
            lat: 45.814464,
            lng: 15.98286,
          },
        },
      ],
      loadMap: false,

      slickOptions: {
        slidesToShow: 1,
      },

      selectedSection: 1,
      selectedPost: -1,
      selectedGallery: false,
      selectedImage: true,
      selectedMap: false,

      metaOfferURL: "153",
      metaOfferNaziv: "",
      metaOfferSlika: "",

      offer: {},
    };
  },
  beforeMount() {
    this.getOffer();
    console.log("skripta");
    function fbShare(url, title, descr, image, winWidth, winHeight) {
      var winTop = screen.height / 2 - winHeight / 2;
      var winLeft = screen.width / 2 - winWidth / 2;
      window.open(
        "http://www.facebook.com/sharer.php?s=100&p[title]=" +
          title +
          "&p[summary]=" +
          descr +
          "&p[url]=" +
          url +
          "&p[images][0]=" +
          image,
        "sharer",
        "top=" +
          winTop +
          ",left=" +
          winLeft +
          ",toolbar=0,status=0,width=" +
          winWidth +
          ",height=" +
          winHeight
      );
    }
    var currentUrl = window.location.href;

    console.log(currentUrl);
    this.url = currentUrl;
  },
  mounted() {
    // this.$ga.page({
    //   path: this.$router.path,
    //   name: this.$router.name,
    //   location: window.location.href,
    // });
  },
  metaInfo: {
    title: "HTZ - Tjedan Odmora Vrijedan",
    meta: [
      {
        name: "description",
        content:
          "Tjedan odmora vrijedan je akcija Ministarstva turizma i sporta i Hrvatske turističke zajednice kojom se u tjednu od 16. do 25. 10. 2020. omogućuje svim građanima Hrvatske da posjete i upoznaju druge dijelove naše zemlje nudeći upola cijene sve turističke proizvode, od smještaja, prijevoza, znamenitosti pa sve do ugostiteljskih usluga i različitih doživljaja.",
      },
      {
        name: "keywords",
        content:
          "Hrvatska turistička zajednica, Ministarstvo turizma i sporta, Tjedan odmora vrijedan, turizam, 2020",
      },
      {
        property: "og:url",
        content: "https://digital.bbdo.hr/HTZ-B2C/ponuda/136",
      },
      { property: "og:type", content: "website" },
      { property: "og:title", content: "Tjedan odmora vrijedan" },
      { property: "og:description", content: "description" },
      {
        property: "og:image",
        content: "https://tjedanodmoravrijedan.hr/static/htz-tov-logo.png",
      },
    ],
  },
  filters: {
    cleanURL(url = "") {
      let newUrl = window.decodeURIComponent(url);
      newUrl = newUrl.trim().replace(/\s/g, "");

      if (/^(:\/\/)/.test(newUrl)) {
        return `https${newUrl}`;
      }

      if (!/^(f|ht)tps?:\/\//i.test(newUrl)) {
        return `https://${newUrl}`;
      }

      return newUrl;
    },
  },
  methods: {
    openLink(e) {
      let x = e.target.getAttribute("href");
      window.open(x, "_blank");
    },
    getLatLng(offer) {
      if (Object.keys(offer.poslovnaJedinica).length > 0) {
        this.center.lat = Number(offer.poslovnaJedinica.latitude);
        this.center.lng = Number(offer.poslovnaJedinica.longitude);
        this.markers[0].position.lat = Number(offer.poslovnaJedinica.latitude);
        this.markers[0].position.lng = Number(offer.poslovnaJedinica.longitude);
      } else {
        this.center.lat = Number(offer.poslovniSubjekt.latitude);
        this.center.lng = Number(offer.poslovniSubjekt.longitude);
        this.markers[0].position.lat = Number(offer.poslovniSubjekt.latitude);
        this.markers[0].position.lng = Number(offer.poslovniSubjekt.longitude);
      }
    },
    googleMaps() {
      this.map = new window.google.maps.Map(this.$refs["map"], {
        center: {
          lat: this.lat,
          lng: this.lng,
        },
        zoom: 15,
      });

      new window.google.maps.Marker({
        position: {
          lat: this.lat,
          lng: this.lng,
        },
        map: this.map,
      });
    },
    getOffer() {
      let idOffer = this.$route.params.id;

      axios({
        method: "GET",
        url: this.$store.state.baseUrl + "/public/ponuda/" + idOffer,
      })
        .then((response) => {
          this.offer = response.data;
          console.log(this.offer);
          this.loadMap = true;
          this.getLatLng(this.offer);

          this.metaOfferURL =
            "https://tjedanodmoravrijedan.hr/ponuda/" + this.offer.id;

          if (Object.keys(this.offer.poslovnaJedinica).length > 0) {
            this.metaOfferNaziv = this.offer.poslovnaJedinica.naziv;
          } else {
            this.metaOfferNaziv = this.offer.poslovniSubjekt.naziv;
          }

          this.metaOfferSlika = this.offer.slikaGlavna.putanjaSlike;
        })
        .catch((error) => {
          console.log(error);
        });
    },
    addclass(e, index) {
      console.log(e);
      this.selectedPost = index;
    },
    // SLICK FUNCTIONS
    next() {
      this.$refs.slick.next();
    },
    prev() {
      this.$refs.slick.prev();
    },
    reInit() {
      // Helpful if you have to deal with v-for to update dynamic lists
      this.$nextTick(() => {
        this.$refs.slick.reSlick();
      });
    },

    // Events listeners
    handleAfterChange(event, slick, currentSlide) {
      // console.log("handleAfterChange", event, slick, currentSlide);
    },
    handleBeforeChange(event, slick, currentSlide, nextSlide) {
      // console.log("handleBeforeChange", event, slick, currentSlide, nextSlide);
    },
    handleBreakpoint(event, slick, breakpoint) {
      // console.log("handleBreakpoint", event, slick, breakpoint);
    },
    handleDestroy(event, slick) {
      // console.log("handleDestroy", event, slick);
    },
    handleEdge(event, slick, direction) {
      // console.log("handleEdge", event, slick, direction);
    },
    handleInit(event, slick) {
      // console.log("handleInit", event, slick);
    },
    handleReInit(event, slick) {
      // console.log("handleReInit", event, slick);
    },
    handleSetPosition(event, slick) {
      // console.log("handleSetPosition", event, slick);
    },
    handleSwipe(event, slick, direction) {
      // console.log("handleSwipe", event, slick, direction);
    },
    handleLazyLoaded(event, slick, image, imageSource) {
      // console.log("handleLazyLoaded", event, slick, image, imageSource);
    },
    handleLazeLoadError(event, slick, image, imageSource) {
      // console.log("handleLazeLoadError", event, slick, image, imageSource);
    },
  },
};
</script>

<style src="../../node_modules/slick-carousel/slick/slick.css"></style>
<style src="../../node_modules/slick-carousel/slick/slick-theme.css"></style>


<style>
.navigation {
  z-index: 1000;
}

/*SLICK*/

.slick-list {
  width: 100% !important;
  /* height: 100% !important;*/
}

.slick-list img {
  display: block;
}

.slick-prev,
.slick-next {
  z-index: 1000;
  max-height: 100%;
  padding: 0 3rem;
  height: 100%;
}

.slick-prev {
  left: 0;
}

.slick-next {
  right: 0;
}

.slick-prev,
.slick-prev:hover,
.slick-prev:focus {
  background: rgb(42, 42, 42);
  background: -moz-linear-gradient(
    90deg,
    rgba(42, 42, 42, 0.5) 0%,
    rgba(42, 42, 42, 0) 100%
  );
  background: -webkit-linear-gradient(
    90deg,
    rgba(42, 42, 42, 0.5) 0%,
    rgba(42, 42, 42, 0) 100%
  );
  background: linear-gradient(
    90deg,
    rgba(42, 42, 42, 0.5) 0%,
    rgba(42, 42, 42, 0) 100%
  );
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#212121", endColorstr="#212121", GradientType=1);
}

.slick-next,
.slick-next:hover,
.slick-next:focus {
  background: rgb(42, 42, 42);
  background: -moz-linear-gradient(
    90deg,
    rgba(42, 42, 42, 0) 0%,
    rgba(42, 42, 42, 0.5) 100%
  );
  background: -webkit-linear-gradient(
    90deg,
    rgba(42, 42, 42, 0) 0%,
    rgba(42, 42, 42, 0.5) 100%
  );
  background: linear-gradient(
    90deg,
    rgba(42, 42, 42, 0) 0%,
    rgba(42, 42, 42, 0.5) 100%
  );
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#424242", endColorstr="#424242", GradientType=1);
}

.slick-prev:before,
.slick-next:before {
  content: "";
  display: block;
  height: 58px;
  width: 32px;
  z-index: 10000;
  position: relative;
}

.slick-prev:before {
  background: url("../assets/strelica-lijevo.svg") no-repeat;
}

.slick-next:before {
  background: url("../assets/strelica-desno.svg") no-repeat;
}
</style>
<style lang="scss" scoped>
#BBDO {
  .right {
    background: none;

    .offer-contact {
      display: none;
    }

    // p {
    //     display: none;
    // }
  }
}

#BBDO_2 {
  .right {
    background: none;

    .offer-contact {
      display: none;
    }
  }
}

#BBDO_3 {
  .right {
    background: none;

    .offer-contact {
      display: none;
    }
  }
}

.home {
  background-color: white;
  min-height: 40vh;
  max-width: 1200px;
  margin: -15vh auto 5vh;
  box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.1);
  padding: 1vh 0 1vh;
}

.cofferHolderSingle {
  margin-top: 55px;

  div.postHolder {
    background-color: #fff;
  }

  div.fullPost {
    width: 100%;
    position: relative;

    .topContentHolder {
      width: 100%;
      height: 60%;
      display: flex;

      .left {
        width: 70%;
        height: 100%;

        div {
          width: 100%;
          height: 100%;
        }

        img {
          /* object-fit: cover; */
          width: 100%;
          height: 100%;
          display: block;
          line-height: 1;
        }
      }

      .right {
        width: 30%;
        background-color: #e5e5e5;
        position: relative;

        a {
          text-decoration: none;
          color: #00386c;
          transition: 0.3s;

          &:hover {
            color: #00386c;
          }
        }

        p {
          line-height: 1.8;

          img {
            padding-right: 0.3rem;
            height: 17px;
          }

          a {
            margin-bottom: 0.5rem;
          }
        }

        .gallery {
          width: 85%;
          height: 150px;
          margin: 7.5% auto 0;
          position: relative;
          cursor: pointer;

          &:hover > p {
            background-color: rgba(0, 0, 0, 0.55);
          }

          img.background {
            width: 100%;
            height: 100%;
            object-fit: cover;
          }

          p {
            position: absolute;
            margin: 0px;
            top: 0;
            left: 0px;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.75);
            transition: 0.5s;

            img {
              width: 40%;
              margin: 30px 30%;
            }
          }
        }

        p {
          text-align: left;
          width: 100%;
          margin-left: 7.5%;
          color: #000000;
        }

        a.googleMap {
          /* position: absolute;
                      bottom: 30px;
                      right: 30px;
                      cursor: pointer; */
          margin: 0 auto 2rem auto;
          background-color: #f1f1f1;
          display: block;
          padding: 2rem;
          cursor: pointer;

          .pinPoint {
            fill: #00386c;
            transition: 0.25s;
          }

          &:hover .pinPoint {
            fill: #cf1f25;
          }

          span {
            display: block;
            font-size: 14px;
            margin-top: 10px;
            color: #00386c;
            transition: 0.25s;
          }

          &:hover span {
            color: #cf1f25;
          }
        }
      }
    }

    .botContentHolder {
      text-align: left;
      color: black;
      padding: 2% 5%;

      .offerTitle {
        font-family: Roboto;
        font-style: normal;
        font-weight: bold;
        font-size: 40px;
        line-height: 47px;
      }

      .offerText {
        font-family: Roboto;
        font-style: normal;
        font-weight: normal;
        font-size: 18px;
        line-height: 21px;

        color: #000000;
      }

      .subText {
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 16px;
        color: #9c9c9c;
      }
    }
  }
}

/* Social Buttons */

.cta-social-group {
  margin-top: 2rem;
}

button.cta-social {
  border: 0;
  background-color: transparent;
  padding: rem 1rem;
}

.cta-social {
  padding: 0.75rem 1rem;
  border: 0;
  color: #ffffff;
  font-size: 0.8rem;
  font-weight: bold;
  line-height: 1;
  text-decoration: none;
  margin-right: 0.5rem;
  text-transform: uppercase;
  transition: 0.4s scale ease-in-out;
  cursor: pointer;

  img {
    margin-right: 0.5rem;
    height: 1rem;
    vertical-align: text-bottom;
  }
}

.cta-social:hover {
  color: #eeeeee;

  img {
    transform: scale(1.1);
  }
}

.cta-social--fb {
  background: #3b5998;
}

.cta-social--tw {
  background: #1da1f2;
}

@media only screen and (max-width: 850px) {
  #app .postHolder .topContentHolder {
    flex-direction: column;

    .left {
      width: 100%;
    }

    .right {
      width: 100%;

      p {
        line-height: 1.8;
        width: 100%;

        img {
          width: 20%;
          margin: auto;
          display: flex;
          padding-top: 1rem;
        }
      }
    }
  }
}

@media only screen and (max-width: 480px) {
  .offerHolderSingle {
    div.postHolder {
      /* height: 280px;
              flex-grow: 1;
              filter: saturate(0.7);
              transition: 0.3s filter linear, 0.3s -webkit-filter linear; */
      width: 100% !important;
    }

    div.fullPost {
      .topContentHolder {
        height: 66%;

        .left {
          width: 100%;
          height: 96%;
        }

        .right {
          width: 95%;
          height: 100%;
          margin: auto;

          .gallery {
            height: 170px;
          }

          p {
            margin-top: 5%;
          }

          a.googleMap {
            position: relative;
            bottom: 15px;
          }
        }
      }

      .botContentHolder {
        margin-top: 3%;
      }
    }
  }
}

/* UL */
.offer-contact {
  list-style: none;
  margin-left: 0;
  padding: 0;
}

.offer-contact__item {
  padding: 0.5rem 0.5rem 0.5rem 1.5rem;
  position: relative;
  text-align: left;
  display: flex;
  align-items: center;
}

.offer-contact__item a {
  cursor: pointer;
}

.offer-contact__item a:hover {
  color: #cf1f25 !important;
}

.offer-contact__item--adresa:before,
.offer-contact__item--telefon:before,
.offer-contact__item--email:before,
.offer-contact__item--web:before {
  content: "";
  background-position: 50% 50%;
  background-size: 90%;
  width: 1.8rem;
  height: 1.8rem;
  margin-right: 0.5rem;
  display: block;
  background-repeat: no-repeat;
}

.offer-contact__item--adresa:before {
  background-image: url(~@/assets/ikona_adresa.svg);
}

.offer-contact__item--telefon:before {
  background-image: url(~@/assets/ikona_telefon.svg);
}

.offer-contact__item--email:before {
  background-image: url(~@/assets/ikona_email.svg);
}

.offer-contact__item--web:before {
  background-image: url(~@/assets/ikona_web.svg);
  background-size: 80%;
}

.right-cta {
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: stretch;
  padding: 2rem 0;
  background-color: #f4f4f4;
}

.right-cta a {
  cursor: pointer;
}

.right-cta__item {
  flex: 0 0 50%;
  max-width: 50%;
  padding: 1rem 0;
  position: relative;
}

.right-cta__item--mapa:after {
  content: "";
  display: block;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  border-right: 1px solid #0f3972;
  width: 1px;
  height: 100%;
}

.right-cta__item a {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  transition: 0.4s color ease-in;
}

.right-cta__item a:hover {
  color: #cf1f25 !important;
}

.right-cta__item svg {
  width: 100%;
  height: 100%;
  max-width: 40px;
  max-height: 40px;
  fill: currentColor;
}

.right-cta__item span {
  display: block;
  margin-top: 0.5rem;
  text-transform: uppercase;
  font-size: 0.8rem;
}
</style>